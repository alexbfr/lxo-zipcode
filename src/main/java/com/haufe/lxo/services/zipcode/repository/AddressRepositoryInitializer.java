
package com.haufe.lxo.services.zipcode.repository;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.haufe.lxo.services.zipcode.repository.csvparsing.CityCsvReader;
import com.haufe.lxo.services.zipcode.repository.csvparsing.FederalStateCsvReader;
import com.haufe.lxo.services.zipcode.repository.csvparsing.RechtskreisCsvReader;
import com.haufe.lxo.services.zipcode.repository.csvparsing.StreetCsvReader;

/**
 * Created by AlexB on 4/21/2016.
 */
@Service
@Transactional
public class AddressRepositoryInitializer {

  private static final Logger LOGGER = LoggerFactory.getLogger(AddressRepositoryInitializer.class);

  @Autowired
  private FederalStateRepository federalStateRepository;

  @Autowired
  private CityRepository cityRepository;

  @Autowired
  private StreetRepository streetRepository;

  @Autowired
  private RechtskreisRepository rechtskreisRepository;

  @PostConstruct
  @Transactional
  public void init()
    throws IOException, InterruptedException {

    HashMap<Integer, FederalState> allFederalStatesById = new HashMap<>();
    HashMap<String, FederalState> allFederalStatesByName = new HashMap<>();

    Map<Integer, City> allCitiesById = new HashMap<>();
    Map<Integer, Street> allStreetsById = new HashMap<>();

    readFederalStates(allFederalStatesById, allFederalStatesByName);
    readRechtskreise(allFederalStatesByName);
    readCities(allCitiesById, allFederalStatesById);
    //readStreets();
  }

  private void readFederalStates(Map<Integer, FederalState> allFederalStatesById, Map<String, FederalState> allFederalStatesByName)
    throws IOException {

    FederalStateCsvReader reader = new FederalStateCsvReader();
    reader.readCsv("/static/KG.txt", federalState -> {
      allFederalStatesByName.put(federalState.getName(), federalState);
      allFederalStatesById.put(federalState.getId(), federalState);
    });
    federalStateRepository.save(allFederalStatesById.values());
  }

  private void readRechtskreise(final Map<String, FederalState> allFederalStatesByName)
    throws IOException {

    List<Rechtskreis> rechtskreise = new ArrayList<Rechtskreis>();
    RechtskreisCsvReader reader = new RechtskreisCsvReader(allFederalStatesByName);
    reader.readCsv("/static/RK.txt", rechtskreis -> rechtskreise.add(rechtskreis));
    rechtskreisRepository.save(rechtskreise);
  }

  private void readCities(Map<Integer, City> allCitiesById, final Map<Integer, FederalState> allFederalStatesById)
    throws IOException {

    CityCsvReader reader = new CityCsvReader(allFederalStatesById);
    reader.readCsv("/static/OR.txt", city -> allCitiesById.put(city.getId(), city));
    cityRepository.save(allCitiesById.values());
  }

  private void readStreets(Map<Integer, Street> allStreetsById, Map<Integer, City> allCitiesById)
    throws IOException {

    StreetCsvReader reader = new StreetCsvReader(allCitiesById);
    reader.readCsv("/static/ST.txt", street -> allStreetsById.put(street.getId(), street));
    streetRepository.save(allStreetsById.values());
  }
}
