
package com.haufe.lxo.services.zipcode.repository;

/**
 * Created by AlexB on 4/21/2016.
 */
public interface StreetRepository extends BaseRepository<Street> {
}
