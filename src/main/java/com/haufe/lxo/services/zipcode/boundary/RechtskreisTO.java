
package com.haufe.lxo.services.zipcode.boundary;

import com.haufe.lxo.services.zipcode.repository.Rechtskreis;
import com.haufe.lxo.services.zipcode.repository.RechtskreisEnum;

/**
 * Created by AlexB on 4/26/2016.
 */
public class RechtskreisTO implements BaseTO<Rechtskreis> {

  private int id;

  private int zipCodeMin;

  private int zipCodeMax;

  private RechtskreisEnum rechtskreis;

  private FederalStateTO federalState;

  public RechtskreisTO() {

  }

  public RechtskreisTO(final Rechtskreis rechtskreis) {
    copyFromEntity(rechtskreis);
  }

  @Override
  public void copyFromEntity(final Rechtskreis entity) {

    id = entity.getId();
    zipCodeMin = entity.getZipCodeMin();
    zipCodeMax = entity.getZipCodeMax();
    rechtskreis = entity.getRechtskreis();
    federalState = new FederalStateTO(entity.getFederalState());
  }

  @Override
  public Rechtskreis copyToEntity() {

    Rechtskreis rechtskreis = new Rechtskreis();
    rechtskreis.setId(id);
    rechtskreis.setZipCodeMin(this.zipCodeMin);
    rechtskreis.setZipCodeMax(this.zipCodeMax);
    rechtskreis.setRechtskreis(this.rechtskreis);
    rechtskreis.setFederalState(this.federalState.copyToEntity());
    return rechtskreis;
  }

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public int getZipCodeMin() {

    return zipCodeMin;
  }

  public void setZipCodeMin(int zipCodeMin) {

    this.zipCodeMin = zipCodeMin;
  }

  public int getZipCodeMax() {

    return zipCodeMax;
  }

  public void setZipCodeMax(int zipCodeMax) {

    this.zipCodeMax = zipCodeMax;
  }

  public RechtskreisEnum getRechtskreis() {

    return rechtskreis;
  }

  public void setRechtskreis(RechtskreisEnum rechtskreis) {

    this.rechtskreis = rechtskreis;
  }

  public FederalStateTO getFederalState() {

    return federalState;
  }

  public void setFederalState(FederalStateTO federalState) {

    this.federalState = federalState;
  }
}
