
package com.haufe.lxo.services.zipcode.repository;

import java.util.List;

import org.springframework.data.repository.query.Param;

/**
 * Created by AlexB on 4/21/2016.
 */
public interface FederalStateRepository extends BaseRepository<FederalState> {

  List<FederalState> findByNameStartingWith(@Param("name") String name);
}
