
package com.haufe.lxo.services.zipcode.repository.csvparsing;

import java.util.Map;

import org.apache.commons.csv.CSVRecord;

import com.haufe.lxo.services.zipcode.repository.City;
import com.haufe.lxo.services.zipcode.repository.Street;

/**
 * Created by AlexB on 4/26/2016.
 */
public class StreetCsvReader extends BaseCsvReader<Street> {

  // ST / "ST_ALORT"
  private static final int streetCityColumn = 0;

  // ST / "ST_PLZ"
  private static final int streetZipColumn = 1;

  // ST / "ST_NAME"
  private static final int streetNameColumn = 7;

  private final Map<Integer, City> allCitiesById;

  public StreetCsvReader(Map<Integer, City> allCitiesById) {
    this.allCitiesById = allCitiesById;
  }

  @Override
  protected Street parseRecord(CSVRecord record) {

    Street street = new Street();

    street.setZipCode(getIntFromRecord(record, streetZipColumn));
    City city = allCitiesById.get(getIntFromRecord(record, streetCityColumn));
    street.setCity(city);
    street.setName(getStringFromRecord(record, streetNameColumn));

    int id = (Integer.toString(city.getId()) + street.getName()).hashCode();
    street.setId(id);
    return street;
  }

}
