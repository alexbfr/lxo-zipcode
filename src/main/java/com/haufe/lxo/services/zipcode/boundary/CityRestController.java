
package com.haufe.lxo.services.zipcode.boundary;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.haufe.lxo.services.zipcode.service.AddressSearchService;

/**
 * Created by AlexB on 4/25/2016.
 */
@RestController
@RequestMapping(value = "/v100/cities", produces = MediaType.APPLICATION_JSON_VALUE)
public class CityRestController {

  @Autowired
  private AddressSearchService addressSearchService;

  @RequestMapping(method = RequestMethod.GET)
  public CityListTO search(@RequestParam("q") String text, @RequestParam(value = "limit", required = false) String limitAsString) {

    int limit = StringUtils.isNumeric(limitAsString) ? Integer.parseInt(limitAsString) : 10;
    return new CityListTO(addressSearchService.findCitiesByNameOrZip(text, limit));
  }
}
