
package com.haufe.lxo.services.zipcode.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.NoRepositoryBean;

/**
 * Created by AlexB on 4/21/2016.
 */
@NoRepositoryBean
public interface BaseRepository<T> extends CrudRepository<T, Integer> {

}
