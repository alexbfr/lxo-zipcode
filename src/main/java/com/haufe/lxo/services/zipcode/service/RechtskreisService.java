
package com.haufe.lxo.services.zipcode.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.haufe.lxo.services.zipcode.repository.Rechtskreis;
import com.haufe.lxo.services.zipcode.repository.RechtskreisRepository;

/**
 * Created by AlexB on 4/26/2016.
 */
@Service
public class RechtskreisService {

  @Autowired
  private RechtskreisRepository rechtskreisRepository;

  public Rechtskreis findRechtskreisByZip(int zipCode) {

    List<Rechtskreis> rechtskreisList = rechtskreisRepository.findByZipCode(zipCode);
    if (rechtskreisList.size() == 0) {
      return null;
    } else {
      return rechtskreisList.get(0);
    }
  }

}
