
package com.haufe.lxo.services.zipcode.repository.csvparsing;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.apache.commons.lang3.StringUtils;

/**
 * Created by AlexB on 4/26/2016.
 */
public abstract class BaseCsvReader<T> {

  protected static class MinMax {

    private int min;
    private int max;

    public int getMin() {

      return min;
    }

    public void setMin(int min) {

      this.min = min;
    }

    public int getMax() {

      return max;
    }

    public void setMax(int max) {

      this.max = max;
    }

    public int getDelta() {

      return max - min;
    }
  }

  public void readCsv(String resourceName, Consumer<T> consumer)
    throws IOException {

    List<T> result = new ArrayList<>();
    try (InputStream resourceStream = ClassLoader.class.getResourceAsStream(resourceName)) {
      CSVFormat csvFormat = CSVFormat.newFormat(',').withQuote('"');
      CSVParser parser = new CSVParser(new InputStreamReader(resourceStream), csvFormat);
      for (CSVRecord record : parser) {
        consumer.accept(parseRecord(record));
      }
    }
  }

  protected abstract T parseRecord(CSVRecord record);

  protected static int getIntFromRecord(CSVRecord record, int index) {

    String input = removeQuotes(record.get(index));
    return StringUtils.isEmpty(input) ? 0 : Integer.parseInt(input);
  }

  protected static String getStringFromRecord(CSVRecord record, int index) {

    return removeQuotes(record.get(index));
  }

  protected static String removeQuotes(String input) {

    int length = input.length();
    if (length > 2 && input.charAt(0) == '"' && input.charAt(length - 1) == '"') {
      return input.substring(1, input.length() - 1);
    } else {
      return input;
    }
  }

  protected static MinMax parseDotSeparatedMinMax(String input) {

    MinMax minMax = new MinMax();
    int indexOfDoubleDot = input.indexOf("..");
    if (indexOfDoubleDot == -1) {
      int inputValue = Integer.parseInt(input);
      minMax.min = inputValue;
      minMax.max = inputValue;
    } else {
      String minPart = input.substring(0, indexOfDoubleDot);
      String maxPart = input.substring(indexOfDoubleDot + 2);
      minMax.min = Integer.parseInt(minPart);
      minMax.max = Integer.parseInt(maxPart);
    }
    return minMax;
  }
}
