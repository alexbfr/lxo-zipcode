
package com.haufe.lxo.services.zipcode.boundary;

import com.haufe.lxo.services.zipcode.repository.City;

/**
 * Created by AlexB on 4/25/2016.
 */
public class CityTO implements BaseTO<City> {

  private int id;

  private String name;

  private FederalStateTO federalState;

  private int zipCodeMin;

  private int zipCodeMax;

  public CityTO(City city) {
    copyFromEntity(city);
  }

  @Override
  public void copyFromEntity(City entity) {

    id = entity.getId();
    name = entity.getName();
    federalState = new FederalStateTO(entity.getFederalState());
    zipCodeMin = entity.getZipCodeMin();
    zipCodeMax = entity.getZipCodeMax();
  }

  @Override
  public City copyToEntity() {

    City city = new City();
    city.setId(id);
    city.setName(name);
    city.setFederalState(federalState.copyToEntity());
    city.setZipCodeMin(zipCodeMin);
    city.setZipCodeMax(zipCodeMax);
    return city;
  }

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public FederalStateTO getFederalState() {

    return federalState;
  }

  public void setFederalState(FederalStateTO federalState) {

    this.federalState = federalState;
  }

  public int getZipCodeMin() {

    return zipCodeMin;
  }

  public void setZipCodeMin(int zipCodeMin) {

    this.zipCodeMin = zipCodeMin;
  }

  public int getZipCodeMax() {

    return zipCodeMax;
  }

  public void setZipCodeMax(int zipCodeMax) {

    this.zipCodeMax = zipCodeMax;
  }
}
