
package com.haufe.lxo.services.zipcode.repository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;

/**
 * Created by AlexB on 4/26/2016.
 */
@Entity
@Access(AccessType.FIELD)
@Table(schema = "zipcode")
public class Rechtskreis {

  @Id
  @GeneratedValue
  private int id;

  @Column
  private int zipCodeMin;

  @Column
  private int zipCodeMax;

  @Column
  private int zipCodeMinMaxDelta;

  @ManyToOne(fetch = FetchType.EAGER)
  private FederalState federalState;

  @Column
  @Enumerated(EnumType.STRING)
  private RechtskreisEnum rechtskreis;

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public int getZipCodeMin() {

    return zipCodeMin;
  }

  public void setZipCodeMin(int zipCodeMin) {

    this.zipCodeMin = zipCodeMin;
  }

  public int getZipCodeMax() {

    return zipCodeMax;
  }

  public void setZipCodeMax(int zipCodeMax) {

    this.zipCodeMax = zipCodeMax;
  }

  public int getZipCodeMinMaxDelta() {

    return zipCodeMinMaxDelta;
  }

  public void setZipCodeMinMaxDelta(int zipCodeMinMaxDelta) {

    this.zipCodeMinMaxDelta = zipCodeMinMaxDelta;
  }

  public FederalState getFederalState() {

    return federalState;
  }

  public void setFederalState(FederalState federalState) {

    this.federalState = federalState;
  }

  public RechtskreisEnum getRechtskreis() {

    return rechtskreis;
  }

  public void setRechtskreis(RechtskreisEnum rechtskreis) {

    this.rechtskreis = rechtskreis;
  }

  @Override
  public boolean equals(Object other) {

    if (other == null || !(other instanceof Rechtskreis)) {
      return false;
    }
    return ((Rechtskreis) other).getId() == getId();
  }

  @Override
  public int hashCode() {

    return ((Integer) getId()).hashCode();
  }

  @Override
  public String toString() {

    return MoreObjects.toStringHelper(this).addValue(zipCodeMin).addValue(zipCodeMax).toString();
  }
}
