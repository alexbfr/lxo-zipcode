
package com.haufe.lxo.services.zipcode.repository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;

/**
 * Created by AlexB on 4/21/2016.
 */
@Entity
@Table(schema = "zipcode")
@Access(AccessType.FIELD)
public class Street {

  @Id
  @Column
  private int id;

  @ManyToOne(fetch = FetchType.EAGER)
  private City city;

  @Column
  private int zipCode;

  @Column
  private String name;

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public City getCity() {

    return city;
  }

  public void setCity(City city) {

    this.city = city;
  }

  public int getZipCode() {

    return zipCode;
  }

  public void setZipCode(int zipCode) {

    this.zipCode = zipCode;
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  @Override
  public boolean equals(Object other) {

    if (other == null || !(other instanceof Street)) {
      return false;
    }
    return ((Street) other).getId() == getId();
  }

  @Override
  public int hashCode() {

    return getName().hashCode() ^ ((Integer) getZipCode()).hashCode();
  }

  @Override
  public String toString() {

    return MoreObjects.toStringHelper(this).addValue(name).addValue(getZipCode()).toString();
  }
}
