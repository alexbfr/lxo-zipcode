
package com.haufe.lxo.services.zipcode.repository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;

/**
 * Created by AlexB on 4/21/2016.
 */
@Entity
@Access(AccessType.FIELD)
@Table(schema = "zipcode")
public class FederalState {

  @Id
  @Column
  private int id;

  @Column
  private String name;

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  @Override
  public boolean equals(Object other) {

    if (other == null || !(other instanceof FederalState)) {
      return false;
    }
    return ((FederalState) other).getId() == getId();
  }

  @Override
  public int hashCode() {

    return getName().hashCode();
  }

  @Override
  public String toString() {

    return MoreObjects.toStringHelper(this).addValue(name).toString();
  }
}
