
package com.haufe.lxo.services.zipcode.boundary;

import java.util.List;
import java.util.stream.Collectors;

import com.haufe.lxo.services.zipcode.repository.City;

/**
 * Created by AlexB on 4/25/2016.
 */
public class CityListTO implements BaseTO<List<City>> {

  private List<CityTO> cities;

  public CityListTO() {

  }

  public CityListTO(List<City> cities) {
    copyFromEntity(cities);
  }

  @Override
  public void copyFromEntity(List<City> entity) {

    cities = entity.stream().map(city -> new CityTO(city)).collect(Collectors.toList());
  }

  @Override
  public List<City> copyToEntity() {

    return cities.stream().map(city -> city.copyToEntity()).collect(Collectors.toList());
  }

  public List<CityTO> getCities() {

    return cities;
  }

  public void setCities(List<CityTO> cities) {

    this.cities = cities;
  }
}
