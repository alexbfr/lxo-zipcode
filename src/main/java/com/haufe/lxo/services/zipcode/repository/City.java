
package com.haufe.lxo.services.zipcode.repository;

import javax.persistence.Access;
import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.google.common.base.MoreObjects;

/**
 * Created by AlexB on 4/21/2016.
 */
@Entity
@Table(schema = "zipcode")
@Access(AccessType.FIELD)
public class City {

  @Id
  @Column
  private int id;

  @Column
  private String name;

  @ManyToOne(fetch = FetchType.EAGER)
  private FederalState federalState;

  @Column
  private int zipCodeMin;

  @Column
  private int zipCodeMax;

  @Column
  private int zipCodeMinMaxDelta;

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }

  public FederalState getFederalState() {

    return federalState;
  }

  public void setFederalState(FederalState federalState) {

    this.federalState = federalState;
  }

  public int getZipCodeMin() {

    return zipCodeMin;
  }

  public void setZipCodeMin(int zipCodeMin) {

    this.zipCodeMin = zipCodeMin;
  }

  public int getZipCodeMax() {

    return zipCodeMax;
  }

  public void setZipCodeMax(int zipCodeMax) {

    this.zipCodeMax = zipCodeMax;
  }

  public int getZipCodeMinMaxDelta() {

    return zipCodeMinMaxDelta;
  }

  public void setZipCodeMinMaxDelta(int zipCodeMinMaxDelta) {

    this.zipCodeMinMaxDelta = zipCodeMinMaxDelta;
  }

  @Override
  public boolean equals(Object other) {

    if (other == null || !(other instanceof City)) {
      return false;
    }
    return ((City) other).getId() == getId();
  }

  @Override
  public int hashCode() {

    return getName().hashCode() ^ getZipCodeMin() ^ getZipCodeMax();
  }

  @Override
  public String toString() {

    return MoreObjects.toStringHelper(this).addValue(name).addValue(zipCodeMin).addValue(zipCodeMax).toString();
  }

  public int compareTo(City other) {

    if (other.getZipCodeMinMaxDelta() < getZipCodeMinMaxDelta()) {
      return -1;
    } else if (other.getZipCodeMinMaxDelta() > getZipCodeMinMaxDelta()) {
      return 1;
    }

    return getName().compareTo(other.getName());
  }
}
