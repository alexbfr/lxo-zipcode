
package com.haufe.lxo.services.zipcode.boundary;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.haufe.lxo.services.zipcode.repository.Rechtskreis;
import com.haufe.lxo.services.zipcode.service.RechtskreisService;

/**
 * Created by AlexB on 4/26/2016.
 */
@RestController
@RequestMapping(value = "/v100/rechtskreise", produces = MediaType.APPLICATION_JSON_VALUE)
public class RechtskreisRestController {

  @Autowired
  private RechtskreisService rechtskreisService;

  @RequestMapping(method = RequestMethod.GET)
  public RechtskreisTO search(@RequestParam("q") String text) {

    int zipCode = Integer.parseInt(text);
    Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(zipCode);
    if (rechtskreis == null) {
      return null;
    } else {
      return new RechtskreisTO(rechtskreis);
    }
  }

}
