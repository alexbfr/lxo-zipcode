
package com.haufe.lxo.services.zipcode.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.google.common.base.Strings;
import com.haufe.lxo.services.zipcode.repository.City;
import com.haufe.lxo.services.zipcode.repository.CityRepository;
import com.haufe.lxo.services.zipcode.repository.FederalState;
import com.haufe.lxo.services.zipcode.repository.FederalStateRepository;

/**
 * Created by AlexB on 4/21/2016.
 */
@Service
public class AddressSearchService {

  @Autowired
  private CityRepository cityRepository;

  @Autowired
  private FederalStateRepository federalStateRepository;

  public List<City> findCitiesByNameOrZip(String text, int limit) {

    String[] words = StringUtils.split(text, " \t");
    String zipcode = findZipCodeIn(words);
    String cityName = findCityNameIn(words);

    HashSet<City> cities = new HashSet<City>();

    cities.addAll(findCitiesByZipCodeStartingWith(zipcode, limit));
    cities.addAll(findCitiesByNameStartingWith(cityName, limit));

    return cities.stream().sorted((city1, city2) -> city1.compareTo(city2)).collect(Collectors.toList());
  }

  public List<City> findCitiesByNameStartingWith(String name, int limit) {

    if (Strings.isNullOrEmpty(name)) {
      return new ArrayList<City>();
    }

    Pageable pageable = new PageRequest(0, limit);
    return cityRepository.findByNameStartingWithOrderByZipCodeMinMaxDeltaDescNameAsc(name, pageable);
  }

  public List<FederalState> findFederalStateByNameStartingWith(String name) {

    return federalStateRepository.findByNameStartingWith(name);
  }

  public List<City> findCitiesByZipCodeStartingWith(String partialZipCodeAsText, int limit) {

    if (Strings.isNullOrEmpty(partialZipCodeAsText)) {
      return new ArrayList<City>();
    }

    int zipCodeLowerBound = 0, zipCodeUpperBound = 0;
    try {
      String zipCodeAsTextInclusiveLowerBound = Strings.padEnd(partialZipCodeAsText, 5, '0');
      String zipCodeAsTextInclusiveUpperBound = Strings.padEnd(partialZipCodeAsText, 5, '9');
      zipCodeLowerBound = Integer.parseInt(zipCodeAsTextInclusiveLowerBound);
      zipCodeUpperBound = Integer.parseInt(zipCodeAsTextInclusiveUpperBound);
    } catch (NumberFormatException ex) {
      return new ArrayList<City>();
    }

    Pageable pageable = new PageRequest(0, limit);
    return cityRepository.findByZipCodeInRange(zipCodeLowerBound, zipCodeUpperBound, pageable);
  }

  private String findZipCodeIn(String[] words) {

    for (String word : words) {
      if (word.length() <= 5 && StringUtils.isNumeric(word)) {
        return word;
      }
    }
    return null;
  }

  private String findCityNameIn(String[] words) {

    for (String word : words) {
      if (word.length() > 1 && !StringUtils.isNumeric(word)) {
        return word;
      }
    }
    return null;
  }
}
