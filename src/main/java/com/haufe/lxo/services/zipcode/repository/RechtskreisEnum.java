
package com.haufe.lxo.services.zipcode.repository;

/**
 * Created by AlexB on 4/26/2016.
 */
public enum RechtskreisEnum {
    EAST("E"),

    WEST("W");

  private final String name;

  private RechtskreisEnum(String name) {
    this.name = name;
  }

  public static RechtskreisEnum getRechtskreisFromString(String name) {

    for (RechtskreisEnum value : RechtskreisEnum.values()) {
      if (value.name.equals(name)) {
        return value;
      }
    }
    return null;
  }
}
