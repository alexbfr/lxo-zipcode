
package com.haufe.lxo.services.zipcode.repository.csvparsing;

import java.util.Map;

import org.apache.commons.csv.CSVRecord;

import com.haufe.lxo.services.zipcode.repository.City;
import com.haufe.lxo.services.zipcode.repository.FederalState;

/**
 * Created by AlexB on 4/26/2016.
 */
public class CityCsvReader extends BaseCsvReader<City> {

  // OR / "OR_ALORT"
  private static final int cityIdColumn = 0;

  // OR / "OR_LAND"
  private static final int cityFederalStateIdColumn = 6;

  // OR / "OR_PLZ"
  private static final int cityZipCodeColumn = 7;

  // OR / "OR_ONAME"
  private static final int cityNameColumn = 1;

  private final Map<Integer, FederalState> allFederalStatesById;

  public CityCsvReader(Map<Integer, FederalState> allFederalStatesById) {
    this.allFederalStatesById = allFederalStatesById;
  }

  @Override
  protected City parseRecord(CSVRecord record) {

    City city = new City();
    city.setId(getIntFromRecord(record, cityIdColumn));

    int federalStateId = getIntFromRecord(record, cityFederalStateIdColumn);
    city.setFederalState(allFederalStatesById.get(federalStateId));

    city.setName(getStringFromRecord(record, cityNameColumn));
    MinMax zipCodeMinMax = parseDotSeparatedMinMax(getStringFromRecord(record, cityZipCodeColumn));
    city.setZipCodeMin(zipCodeMinMax.getMin());
    city.setZipCodeMax(zipCodeMinMax.getMax());
    city.setZipCodeMinMaxDelta(zipCodeMinMax.getDelta());
    return city;
  }
}
