
package com.haufe.lxo.services.zipcode.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by AlexB on 4/26/2016.
 */
public interface RechtskreisRepository extends BaseRepository<Rechtskreis> {

  @Query(value = "select top 1 * from zipcode.rechtskreis where zipCodeMin <= :zipcode and zipCodeMax >= :zipcode order by zipCodeMinMaxDelta", nativeQuery = true)
  List<Rechtskreis> findByZipCode(@Param("zipcode") int zipcode);
}
