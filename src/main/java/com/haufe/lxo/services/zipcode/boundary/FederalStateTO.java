
package com.haufe.lxo.services.zipcode.boundary;

import com.haufe.lxo.services.zipcode.repository.FederalState;

/**
 * Created by AlexB on 4/25/2016.
 */
public class FederalStateTO implements BaseTO<FederalState> {

  private int id;

  private String name;

  public FederalStateTO() {

  }

  public FederalStateTO(FederalState federalState) {
    copyFromEntity(federalState);
  }

  @Override
  public void copyFromEntity(FederalState entity) {

    id = entity.getId();
    name = entity.getName();
  }

  @Override
  public FederalState copyToEntity() {

    FederalState federalState = new FederalState();
    federalState.setId(id);
    federalState.setName(name);
    return federalState;
  }

  public int getId() {

    return id;
  }

  public void setId(int id) {

    this.id = id;
  }

  public String getName() {

    return name;
  }

  public void setName(String name) {

    this.name = name;
  }
}
