
package com.haufe.lxo.services.zipcode.boundary;

/**
 * Created by AlexB on 4/21/2016.
 */
public interface BaseTO<T> {

  void copyFromEntity(T entity);

  T copyToEntity();

}
