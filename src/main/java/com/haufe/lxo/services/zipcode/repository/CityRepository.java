
package com.haufe.lxo.services.zipcode.repository;

import java.util.List;

import org.hibernate.annotations.OrderBy;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by AlexB on 4/21/2016.
 */
public interface CityRepository extends BaseRepository<City> {

  @OrderBy(clause = "zipCodeMinMaxDelta desc, name asc")
  List<City> findByNameStartingWithOrderByZipCodeMinMaxDeltaDescNameAsc(@Param("name") String name);

  @OrderBy(clause = "zipCodeMinMaxDelta desc, name asc")
  List<City> findByNameStartingWithOrderByZipCodeMinMaxDeltaDescNameAsc(@Param("name") String name, Pageable pageable);

  @Query("select c from City c where c.zipCodeMin <= :zipCodeMax and c.zipCodeMax >= :zipCodeMin order by zipCodeMinMaxDelta desc, name asc")
  List<City> findByZipCodeInRange(@Param("zipCodeMin") int zipCodeMin, @Param("zipCodeMax") int zipCodeMax);

  @Query("select c from City c where c.zipCodeMin <= :zipCodeMax and c.zipCodeMax >= :zipCodeMin order by zipCodeMinMaxDelta desc, name asc")
  List<City> findByZipCodeInRange(@Param("zipCodeMin") int zipCodeMin, @Param("zipCodeMax") int zipCodeMax, Pageable pageable);

}
