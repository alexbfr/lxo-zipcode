
package com.haufe.lxo.services.zipcode.repository.csvparsing;

import java.util.Map;

import org.apache.commons.csv.CSVRecord;

import com.haufe.lxo.services.zipcode.repository.FederalState;
import com.haufe.lxo.services.zipcode.repository.Rechtskreis;
import com.haufe.lxo.services.zipcode.repository.RechtskreisEnum;

/**
 * Created by AlexB on 4/26/2016.
 */
public class RechtskreisCsvReader extends BaseCsvReader<Rechtskreis> {

  // Rechtskreis - PLZ
  private static final int rechtskreisZipColumn = 0;

  // Rechtskreis - Bundesland
  private static final int rechtskreisFederalStateNameColumn = 1;

  // Rechtskreis - Bundeslandkürzel
  private static final int rechtskreisFederalStateShortNameColumn = 2;

  // Rechtskreis - Rechtskreis
  private static final int rechtskreisRechtskreisColumn = 3;

  private final Map<String, FederalState> allFederalStatesByName;

  public RechtskreisCsvReader(Map<String, FederalState> allFederalStatesByName) {
    this.allFederalStatesByName = allFederalStatesByName;
  }

  @Override
  protected Rechtskreis parseRecord(CSVRecord record) {

    Rechtskreis rechtskreis = new Rechtskreis();
    FederalState federalState = allFederalStatesByName.getOrDefault(getStringFromRecord(record, rechtskreisFederalStateNameColumn), null);
    if (federalState == null) {
      rechtskreis.setFederalState(null);
    }

    MinMax zipCodeMinMax = parseDotSeparatedMinMax(getStringFromRecord(record, rechtskreisZipColumn));
    rechtskreis.setId(0);
    rechtskreis.setZipCodeMin(zipCodeMinMax.getMin());
    rechtskreis.setZipCodeMax(zipCodeMinMax.getMax());
    rechtskreis.setZipCodeMinMaxDelta(zipCodeMinMax.getDelta());
    rechtskreis.setFederalState(federalState);
    rechtskreis.setRechtskreis(RechtskreisEnum.getRechtskreisFromString(getStringFromRecord(record, rechtskreisRechtskreisColumn)));
    return rechtskreis;
  }

}
