
package com.haufe.lxo.services.zipcode.repository.csvparsing;

import org.apache.commons.csv.CSVRecord;

import com.haufe.lxo.services.zipcode.repository.FederalState;

/**
 * Created by AlexB on 4/26/2016.
 */
public class FederalStateCsvReader extends BaseCsvReader<FederalState> {

  // KG / "KG_SCHLUESSEL"
  private static final int federalStateIdColumn = 0;

  // KG / "KG_NAME"
  private static final int federalStateNameColumn = 1;

  @Override
  protected FederalState parseRecord(CSVRecord record) {

    FederalState federalState = new FederalState();
    federalState.setId(getIntFromRecord(record, federalStateIdColumn));
    federalState.setName(getStringFromRecord(record, federalStateNameColumn));
    return federalState;
  }
}
