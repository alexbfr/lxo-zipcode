CREATE SCHEMA zipcode;

CREATE TABLE zipcode.federalstate (
  id   INTEGER      NOT NULL,
  name VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE zipcode.rechtskreis (
  id                 INTEGER AUTO_INCREMENT,
  zipCodeMin         INTEGER     NOT NULL,
  zipCodeMax         INTEGER     NOT NULL,
  zipCodeMinMaxDelta INTEGER     NOT NULL,
  federalState_Id    INTEGER,
  rechtskreis        VARCHAR(10) NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE zipcode.city (
  id                 INTEGER      NOT NULL,
  name               VARCHAR(255) NOT NULL,
  federalState_Id    INTEGER,
  zipCodeMin         INTEGER      NOT NULL,
  zipCodeMax         INTEGER      NOT NULL,
  zipCodeMinMaxDelta INTEGER      NOT NULL,
  PRIMARY KEY (id)
);

CREATE TABLE zipcode.street (
  id      INTEGER      NOT NULL,
  city_Id INTEGER,
  zipCode INTEGER      NOT NULL,
  name    VARCHAR(255) NOT NULL,
  PRIMARY KEY (id)
);

CREATE INDEX IDX6144468E9195C7E0
ON zipcode.city (zipCodeMin);

CREATE INDEX IDX6144468E9195C7E4
ON zipcode.city (zipCodeMinMaxDelta);

CREATE HASH INDEX IDX6144468E9195C7E1
ON zipcode.city (NAME);

CREATE INDEX IDX9F985A5FEC88EAA8
ON zipcode.rechtskreis (zipCodeMin);

CREATE INDEX IDX6144468E9195C7E2
ON zipcode.street (name);

CREATE INDEX IDX6144468E9195C7E3
ON zipcode.street (zipCode);

ALTER TABLE zipcode.rechtskreis
ADD CONSTRAINT FK83311CFE3AAF61B9
FOREIGN KEY (federalState_Id)
REFERENCES federalstate;

ALTER TABLE zipcode.city
ADD CONSTRAINT FK6144468E9195C7DE
FOREIGN KEY (federalState_Id)
REFERENCES federalstate;

ALTER TABLE zipcode.street
ADD CONSTRAINT FKF40D620F37FEBC08
FOREIGN KEY (city_Id)
REFERENCES city;
