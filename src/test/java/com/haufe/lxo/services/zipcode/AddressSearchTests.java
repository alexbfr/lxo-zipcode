
package com.haufe.lxo.services.zipcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.haufe.lxo.services.zipcode.repository.City;
import com.haufe.lxo.services.zipcode.service.AddressSearchService;

/**
 * Created by AlexB on 4/26/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class AddressSearchTests {

  @Autowired
  AddressSearchService addressSearchService;

  @Test
  public void findFreiburgByZipCode() {
    List<City> cities = addressSearchService.findCitiesByZipCodeStartingWith("79110", 10);
    assertThat(cities).hasSize(1);
    assertThat(cities.get(0).getName()).isEqualTo("Freiburg im Breisgau");
  }

  @Test
  public void findBerlinByZipCode() {
    List<City> cities = addressSearchService.findCitiesByZipCodeStartingWith("10115", 10);
    assertThat(cities).hasSize(1);
    assertThat(cities.get(0).getName()).isEqualTo("Berlin");
  }

  @Test
  public void find5CitiesStartingWithZipCode8() {
    List<City> cities = addressSearchService.findCitiesByZipCodeStartingWith("8", 5);
    assertThat(cities).hasSize(5);
    assertThat(cities.get(0).getName()).isEqualTo("München");
    assertThat(cities.get(1).getName()).isEqualTo("Augsburg (Bayern)");
    assertThat(cities.get(2).getName()).isEqualTo("Ingolstadt (Donau)");
    assertThat(cities.get(3).getName()).isEqualTo("Landshut (Isar)");
    assertThat(cities.get(4).getName()).isEqualTo("Ulm (Donau)");
  }

  @Test
  public void find5CitiesStartingWithZipCode7() {
    List<City> cities = addressSearchService.findCitiesByZipCodeStartingWith("7", 5);
    assertThat(cities).hasSize(5);
    assertThat(cities.get(0).getName()).isEqualTo("Schramberg");
    assertThat(cities.get(1).getName()).isEqualTo("Stuttgart");
    assertThat(cities.get(2).getName()).isEqualTo("Karlsruhe (Baden)");
    assertThat(cities.get(3).getName()).isEqualTo("Freiburg im Breisgau");
    assertThat(cities.get(4).getName()).isEqualTo("Reutlingen");
  }

  @Test
  public void find5CitiesStartingWithZipCode79() {
    List<City> cities = addressSearchService.findCitiesByZipCodeStartingWith("79", 5);
    assertThat(cities).hasSize(5);
    assertThat(cities.get(0).getName()).isEqualTo("Freiburg im Breisgau");
    assertThat(cities.get(1).getName()).isEqualTo("Lörrach");
    assertThat(cities.get(2).getName()).isEqualTo("Aitern");
    assertThat(cities.get(3).getName()).isEqualTo("Albbruck");
    assertThat(cities.get(4).getName()).isEqualTo("Au (Breisgau)");
  }

  @Test
  public void find5CitiesStartingWithBe() {
    List<City> cities = addressSearchService.findCitiesByNameStartingWith("Be", 5);
    assertThat(cities).hasSize(5);
    assertThat(cities.get(0).getName()).isEqualTo("Berlin");
    assertThat(cities.get(1).getName()).isEqualTo("Bergisch Gladbach");
    assertThat(cities.get(2).getName()).isEqualTo("Bergheim (Erft)");
    assertThat(cities.get(3).getName()).isEqualTo("Bebendorf");
    assertThat(cities.get(4).getName()).isEqualTo("Bebensee");
  }

  @Test
  public void find5CitiesByFreeTextSearchFor79() {
    List<City> cities = addressSearchService.findCitiesByNameOrZip("79", 5);
    assertThat(cities).hasSize(5);
    assertThat(cities.get(0).getName()).isEqualTo("Freiburg im Breisgau");
    assertThat(cities.get(1).getName()).isEqualTo("Lörrach");
    assertThat(cities.get(2).getName()).isEqualTo("Aitern");
    assertThat(cities.get(3).getName()).isEqualTo("Albbruck");
    assertThat(cities.get(4).getName()).isEqualTo("Au (Breisgau)");
  }

  @Test
  public void find5CitiesByFreeTextSearchForBe() {
    List<City> cities = addressSearchService.findCitiesByNameOrZip("Be", 5);
    assertThat(cities).hasSize(5);
    assertThat(cities.get(0).getName()).isEqualTo("Berlin");
    assertThat(cities.get(1).getName()).isEqualTo("Bergisch Gladbach");
    assertThat(cities.get(2).getName()).isEqualTo("Bergheim (Erft)");
    assertThat(cities.get(3).getName()).isEqualTo("Bebendorf");
    assertThat(cities.get(4).getName()).isEqualTo("Bebensee");
  }


}
