
package com.haufe.lxo.services.zipcode;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.haufe.lxo.services.zipcode.repository.Rechtskreis;
import com.haufe.lxo.services.zipcode.repository.RechtskreisEnum;
import com.haufe.lxo.services.zipcode.service.RechtskreisService;

/**
 * Created by AlexB on 4/26/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class EastBerlinTests {

  @Autowired
  RechtskreisService rechtskreisService;

  private static final Integer[] eastBerlinZipCodes = {
    // source: https://www.tk.de/centaurus/servlet/contentblob/643494/Datei/74614/Postleitzahlen-Ost-Berlin.pdf 
    10115, // Mitte
    10117, // Mitte
    10119, // Mitte
    10178, // Mitte
    10179, // Mitte
    10243, // Friedrichshain
    10245, // Friedrichshain
    10247, // Friedrichshain
    10247, // Prenzlauer Berg
    10249, // Friedrichshain
    10315, // Friedrichsfelde
    10317, // Friedrichsfelde
    10318, // Karlshorst
    10319, // Friedrichsfelde
    10365, // Lichtenberg
    10367, // Lichtenberg
    10369, // Lichtenberg
    10405, // Prenzlauer Berg
    10407, // Prenzlauer Berg
    10409, // Prenzlauer Berg
    10435, // Mitte
    10437, // Prenzlauer Berg
    10439, // Pankow
    12435, // Alt-Treptow
    12437, // Baumschulenweg
    12439, // Niederschöneweide
    12459, // Oberschöneweide
    12487, // Adlershof
    12487, // Johannisthal
    12489, // Adlershof
    12524, // Altglienicke
    12526, // Bohnsdorf
    12527, // Grünau
    12527, // Karolinenhof
    12527, // Schmöckwitz
    12555, // Köpenick
    12557, // Köpenick
    12559, // Köpenick
    12587, // Friedrichshagen
    12589, // Hessenwinkel
    12619, // Hellersdorf
    12621, // Kaulsdorf
    12623, // Kaulsdorf
    12627, // Hellersdorf
    12629, // Hellersdorf
    12679, // Marzahn
    12681, // Marzahn
    12683, // Biesdorf
    12685, // Marzahn
    12687, // Marzahn
    12689, // Marzahn
    13051, // Hohenschönhausen
    13053, // Hohenschönhausen
    13055, // Hohenschönhausen
    13057, // Falkenberg
    13059, // Hohenschönhausen
    13086, // Weißensee
    13088, // Stadtrandsiedlung Malchow
    13089, // Heinersdorf
    13125, // Buch
    13127, // Buchholz
    13129, // Blankenburg
    13156, // Niederschönhausen
    13158, // Niederschönhausen
    13159, // Blankenfelde
    13187, // Pankow
    13189, // Pankow
  };

  @Test
  public void rechtskreisOfEastBerlinZipCodesIsEast() {

    for (int zipCode : eastBerlinZipCodes) {
      Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(zipCode);
      assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.EAST);
      assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Berlin");
    }
  }

  @Test
  public void rechtskreisOfWestBerlinZipCodesIsWest() {

    Set<Integer> eastBerlinZipCodesSet = new HashSet<Integer>(Arrays.asList(eastBerlinZipCodes));

    for (int zipCode = 10001; zipCode <= 14330; ++zipCode) {
      if (!eastBerlinZipCodesSet.contains(zipCode)) {
        Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(zipCode);
        assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.WEST);
        assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Berlin");
      }
    }
  }
}
