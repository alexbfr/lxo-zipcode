package com.haufe.lxo.services.zipcode;

import static org.assertj.core.api.Assertions.assertThat;

import com.haufe.lxo.services.zipcode.repository.Rechtskreis;
import com.haufe.lxo.services.zipcode.repository.RechtskreisEnum;
import com.haufe.lxo.services.zipcode.service.RechtskreisService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * Created by AlexB on 4/27/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = Application.class)
public class GenericRechtskreisTests {

  @Autowired
  RechtskreisService rechtskreisService;

  @Test
  public void rechtskreisOfFreiburgIsWest() {
    Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(79110);
    assertThat(rechtskreis).isNotNull();
    assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.WEST);
    assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Baden-Württemberg");
  }

  @Test
  public void rechtskreisOfDresdenIsEast() {
    Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(1099);
    assertThat(rechtskreis).isNotNull();
    assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.EAST);
    assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Sachsen");
  }

  @Test
  public void rechtskreisOfHamburgIsWest() {
    Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(20095);
    assertThat(rechtskreis).isNotNull();
    assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.WEST);
    assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Hamburg");
  }

  @Test
  public void rechtskreisOfFrankfurtIsWest() {
    Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(60306);
    assertThat(rechtskreis).isNotNull();
    assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.WEST);
    assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Hessen");
  }

  @Test
  public void rechtskreisOfChemnitzIsEast() {
    Rechtskreis rechtskreis = rechtskreisService.findRechtskreisByZip(9111);
    assertThat(rechtskreis).isNotNull();
    assertThat(rechtskreis.getRechtskreis()).isEqualTo(RechtskreisEnum.EAST);
    assertThat(rechtskreis.getFederalState().getName()).isEqualTo("Sachsen");
  }
}
